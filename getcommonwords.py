#!/usr/bin/python
def remove_ingeds(wordsnotindb, wordsdb):
	wordsindb = set([])
	for word in wordsnotindb[:]:
		if word.endswith('s') or word.endswith('d') or word.endswith('ed') or word.endswith('ing'):
			try: 
				if word.endswith('s') or word.endswith('d'):
					if word[0:len(word) - 1] in wordsdb:
						wordsnotindb.remove(word)
						wordsindb.add(word[0:len(word) - 1])
				if word.endswith('ed'):
					if word[0:len(word) - 2] in wordsdb:
						wordsnotindb.remove(word)
						wordsindb.add(word[0:len(word) - 2])
				if word.endswith('ing'):
					if word[0:len(word) - 3] in wordsdb:
						wordsnotindb.remove(word)
						wordsindb.add(word[0:len(word) - 3])
			except ValueError:
				pass
	return list(wordsindb)

if __name__ == '__main__':
	import sys

	if len(sys.argv) < 5:
		print 'invalid arguments! usage: 1. wordsdb 2. new text 3. wordsnotindb 4. wordsindbnew  please try again'
		quit()
	
	db_fd = open(sys.argv[1], 'r')
	newtext_fd = open(sys.argv[2], 'r')
	wordsnotindb_fd = open(sys.argv[3], 'w')
	wordsindbnew_fd = open(sys.argv[4], 'w')

	wordsdb = db_fd.read()
	wordsnewtext = newtext_fd.read()

	wordsdb = wordsdb.replace('\r', '\n')
	wordsnewtext = wordsnewtext.replace('\r', '\n')

	wordsdb = wordsdb.split('\n')
	wordsnewtext = wordsnewtext.split('\n')

	# get words that are not in words database before considering ing ed s etc.
	wordsnotindb = list(set(wordsnewtext) - set(wordsdb)) 
	wordsnewtext = list(set(wordsnewtext).difference(set(wordsnotindb)))

	# find words that are not in words database as they are end with ing ed s etc, and wordsdb will store 
	# those words in their proper form.
	wordsindb = remove_ingeds(wordsnotindb, wordsdb)

	# remove words that are not in words database from wordsnewtext, and add the others into wordsnewtext
	wordsnewtext = list(set(wordsindb).union(set(wordsnewtext)))

	# write all the words that are not in the words database into file - wordsnotindb.txt
	wordsnotindb_fd.write('\n'.join(wordsnotindb))

	# write all the words that are in the words database into file - wordsindbnew.txt
	wordsindbnew_fd.write('\n'.join(wordsnewtext[1:len(wordsnewtext)]))

	db_fd.close()
	newtext_fd.close()
	wordsnotindb_fd.close()
	wordsindbnew_fd.close()


