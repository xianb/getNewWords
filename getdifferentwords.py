#!/usr/bin/python
if __name__ == '__main__':
	import sys
	
	file1_fd = open(sys.argv[1], 'r')
	file2_fd = open(sys.argv[2], 'r')
	file3_fd = open(sys.argv[3], 'w')

	list1 = file1_fd.read().split()
	list2 = file2_fd.read().split()

	list3 = list(set(list1)- set(list2))
	list3 = '\n'.join(list3)

	file3_fd.write(list3)

	file1_fd.close()
	file2_fd.close()
	file3_fd.close()

