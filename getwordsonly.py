import re
import sys

fd = open(sys.argv[1], 'r')
fd1 = open(sys.argv[2], 'w')

text = fd.read()
start = text.find('=== Commonest 1-words ===')
end = text.find('=== Commonest 2-words ===')
pattern = re.compile(r'(?<=\s)[a-z\'\-]+(?=\s)',re.M)
words = pattern.findall(text[start:end])

result = ' '.join(words)
result = result.replace(' ', '\n')

fd1.write(result)

fd.close()
fd1.close()
#print result
#print result.count('\n') + 1
