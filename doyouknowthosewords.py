#!/usr/bin/python
if __name__ == '__main__':
	import sys
	
	allwords_fd = open(sys.argv[1], 'r')
	unknownwords_fd = open(sys.argv[2], 'a')
	unknownwords = []

	allwords = allwords_fd.read().split()
	
	for i in range(len(allwords)/20 + 1):
		if len(allwords) < 20:
			list20 = allwords[i * 20:i * 20 + len(allwords)]
		else:
			list20 = allwords[i * 20:i * 20 + 20]

		for i in range(len(list20)):
			print str(i) + ' ' + list20[i]

		print'\ndo you know all these 20 words above?'
		while True:
			resp = raw_input('please enter yes or no: ')
			if resp == 'yes' or resp == 'y':
				break
			elif resp == 'no' or resp == 'n':
				resp = raw_input('\nwhich ones are new to you?: ')
				resp = resp.split()
				resp =  map(int, resp)

				words = filter(lambda x:list20.index(x) in resp, list20)
				unknownwords = unknownwords + words
				words = ' '.join(words)
				unknownwords_fd.write(words)
				unknownwords_fd.write(' ')
				break;
			else: 
				print 'please enter yes or no. try again.'

	print unknownwords

