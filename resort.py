import re
import sys

fd = open(sys.argv[1], 'r')
fd1 = open(sys.argv[1][0:sys.argv[1].find('.txt')] + '_reverse.txt', 'w')
wordslist = fd.read()
wordslist = wordslist.replace('\r', '\n')

wordslist = wordslist.split('\n')
wordslist.reverse()

wordslist = ' '.join(wordslist)
wordslist = wordslist.replace('  ', '\n')

fd1.write(wordslist)
fd.close()
fd1.close()

